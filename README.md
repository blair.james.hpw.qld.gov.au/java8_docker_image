## Java8_docker_image
###### Please send any questions, queries or concerns to: `java8docker@blairjames.com`
- OpenJDK8 and Maven 3.6.3 for Java Development.
- Static and Stable to avoid instability and inconsistency.
- CI/CD built, monitored and tested regularly.
- Clean, single concern container.

#### Usage:
``` 
docker run -it --rm blairy/java8 
```

#### Example Commands:
 - docker run -it --rm blairy/java8 java -version
 - docker run -it --rm blairy/java8 mvn -version


